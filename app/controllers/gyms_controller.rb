class GymsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
	before_action :find_gym, only: [:show, :edit, :update, :destroy]

	def index
		if params[:city].blank?
			@gyms = Gym.all.order("created_at DESC")
		else
			@city_id = City.find_by(name: params[:city]).id
			@gyms = Gym.where(city_id: @city_id).order("created_at DESC")
		end
	end

	def show
	end

	def new
		@gym = Gym.new
	end

	def create
		@gym = Gym.new(gyms_params)

		if @gym.save
			redirect_to @gym
		else
			render "New"
		end
	end

	def edit
	end

	def update
		if @gym.update(gyms_params)
			redirect_to @gym
		else
			render "Edit"
		end
	end

	def destroy
		@gym.destroy
		redirect_to root_path
	end

	private

	def gyms_params
		params.require(:gym).permit(:name, :latitude, :longitude, :address, :city_id)
	end

	def find_gym
		@gym = Gym.find(params[:id])
	end
end
