class AddCityIdToGyms < ActiveRecord::Migration
  def change
    add_column :gyms, :city_id, :integer
  end
end
